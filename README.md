# Email Tool – Fullstack/Backend Website Development

To complete this challenge you should :
option 1 - build a **python backend** of an Email tool plataform, using the existing templates with only basic information.
or
option 2 - build a **python backend** of an Email tool plataform and apply the mockups that we will provide you.

## Instructions Option 1 and 2

1 - Clone or download the code skeleton on git@bitbucket.org:AngryVenturesInc/email-tool-backend-fullstack-challenge.git .

2 - Do all the changes that you need to run the project - it is built on Django Framework (https://www.djangoproject.com/start/).

  ## Features that you must include
      - User registration – take in consideration the password security
      - User Login - required to access views
      - User Logout
      - Area to send an email with title, receiver, body and its confirmation

3 - We suggest that you use an gmail account to send the email and reuse settings structure.

4 - After building the differents views, you should send your resolution to hi@angryventures.com (zip or your own git repository).
The email subject should be "YOUR NAME_Fullstack/Backend Challenge"

## Instructions only for Option 2
Beyond the previous instructions:
2 – On the ***mockups*** folder you will find the mockups that you should use to build the frontend.
3 – On the ***images*** folder you will found the background.
4 – On the ***icones*** folder you will found the icons that appear on mockups.


If we need any additional commands to test your resolutions, please explain them in the e-mail or in readme.md file.

### If you’d like to bring any more features or improvements to the table, feel free to do it!
